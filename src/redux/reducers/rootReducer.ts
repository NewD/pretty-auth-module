import { combineReducers } from 'redux'
import { chatReducer } from '../../modules/chat/redux/reducers/chatReducer'

export const rootReducer = combineReducers({
  chat: chatReducer,
})

export type AppState = ReturnType<typeof rootReducer>
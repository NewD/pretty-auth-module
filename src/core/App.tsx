import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import * as modules from '../modules'
import { IModule } from '../interfaces/module'

export type authType = 'login' | 'register'

function App() {
	return (
		<Router>
			<Switch>
				{Object.values(modules).map((module: IModule) => (
					<Route
						path={module.path}
						component={module.component}
						key={module.name}
						exact={module.exact}
					/>
				))}
			</Switch>
		</Router>
	)
}

export default App

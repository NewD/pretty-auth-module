import { FC } from 'react'

export interface IModule {
	name: string
	path: string
	component: FC
	exact?: boolean
}

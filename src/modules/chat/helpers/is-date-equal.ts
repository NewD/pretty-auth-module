export function isDateEqual(date1: Date, date2: Date): boolean {
	// year compare:
	if (date1.getFullYear() !== date2.getFullYear()) {
		return false
	}

	if (date1.getMonth() !== date2.getMonth()) {
		return false
	}

	if (date1.getDay() !== date2.getDay()) {
		return false
	}

	return true
}

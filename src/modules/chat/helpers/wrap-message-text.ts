export function wrapMessageText(
	text: string,
	maxSymbolCount: number
): Array<string> | string {
	if (text.length <= maxSymbolCount) return text

	const words: Array<string> = text.split(/\.|,|!|\?|:|;|\(|\)/)

	console.log(words)

	return words
}

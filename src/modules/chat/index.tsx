import React from 'react'
import { Container, Typography } from '@material-ui/core'
import { ChatContainer } from './components/chat-container'
import { WebSocketProvider } from './providers/websocket'

export const chatModule = {
	name: 'chat',
	path: '/chat',
	component: ChatModule,
}

function ChatModule() {
	return (
		<Container maxWidth="md">
			<Typography variant="h4">CHAT</Typography>

			<WebSocketProvider wsEndpoint="ws://localhost:3001">
				<ChatContainer />
			</WebSocketProvider>
		</Container>
	)
}

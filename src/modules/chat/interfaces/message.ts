export interface IMessage {
	author: string
	receiver: string
	text: string
	date: Date
	isOwn: boolean
}

import { IMessage } from '../interfaces/message'
import { ChatActionTypes, SEND_MESSAGE } from './types'



export function sendMessageAction(message: IMessage, ws: WebSocket): ChatActionTypes {
	ws.send(JSON.stringify(message))

	return {
		type: SEND_MESSAGE,
		payload: message,
	}
}

import { IMessage } from '../interfaces/message'

export const SEND_MESSAGE = 'CHAT/SEND_MESSAGE'
interface ISendMessageAction {
	type: typeof SEND_MESSAGE
	payload: IMessage
}

export type ChatActionTypes = ISendMessageAction

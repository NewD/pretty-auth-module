import { IMessage } from '../../interfaces/message'
import { ChatActionTypes, SEND_MESSAGE } from '../types'

interface IChatState {
	messages: IMessage[]
}

const initialState: IChatState = {
	messages: [],
}

export function chatReducer(
	state: IChatState = initialState,
	action: ChatActionTypes
): IChatState {
	switch (action.type) {
		case SEND_MESSAGE: {
			const messages = state.messages
			messages.push(action.payload)

			return { ...state, messages }
		}
		default: {
			return state
		}
	}
}

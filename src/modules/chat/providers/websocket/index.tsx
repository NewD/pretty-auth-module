import React, { createContext, ReactElement, useContext } from 'react'

export const WebSocketContext = createContext<WebSocket | null>(null)

interface IProps {
	children: ReactElement
	wsEndpoint: string
}

export function WebSocketProvider({ children, wsEndpoint }: IProps) {
	return (
		<WebSocketContext.Provider value={new WebSocket(wsEndpoint)}>
			{children}
		</WebSocketContext.Provider>
	)
}

export const useWS = () => useContext(WebSocketContext)

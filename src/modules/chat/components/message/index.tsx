import React from 'react'
import { Card, Typography } from '@material-ui/core'
import { messageStyles } from './style'
import { IMessage } from '../../interfaces/message'
import { isDateEqual } from '../../helpers/is-date-equal'
import { wrapMessageText } from '../../helpers/wrap-message-text'

export function Message({ author, text, date, isOwn }: IMessage) {
	const classes = messageStyles()
	const currentMessageStyle = isOwn ? classes.myMessage : classes.Message

	const curDate = new Date()
	const messageDate = isDateEqual(curDate, date)
		? `${date.getHours()}:${date.getMinutes()}`
		: `${date.getHours()}:${date.getMinutes()} ${date.getMonth()}/${date.getDay()}`

	const messageText = wrapMessageText(text, 30)

	return (
		<Card className={[classes.wrapper, currentMessageStyle].join(' ')}>
			<Typography className={classes.authorName} variant="button">
				{author}
			</Typography>

			<Typography className={classes.messageText} variant="subtitle1">
				{messageText}
			</Typography>
			<Typography className={classes.messageDate} variant="caption">
				{messageDate}
			</Typography>
		</Card>
	)
}

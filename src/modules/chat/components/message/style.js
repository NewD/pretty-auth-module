import { makeStyles } from '@material-ui/core'

export const messageStyles = makeStyles({
	wrapper: {
		position: 'relative',
		padding: '6px 10px',
		marginBottom: '16px',
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		width: '80%',
		wordWrap: "break-word"
	},

	authorName: {
		position: 'absolute',
		top: '2px',
		left: '16px',
	},

	messageText: {
		width: '80%',
		whiteSpace: 'pre-wrap',
		margin: '16px 0 8px 0',
	},

	messageDate: {
		alignSelf: 'flex-end',
		position: 'absolute',
		right: '8px',
		bottom: '4px',
	},

	myMessage: {
		backgroundColor: 'lightblue',
		alignSelf: 'flex-end',
	},

	Message: {
		backgroundColor: 'lightgrey',
		alignSelf: 'flex-start',
	},
})

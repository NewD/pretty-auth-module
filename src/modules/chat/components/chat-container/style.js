import { makeStyles } from '@material-ui/core'

export const chatStyles = makeStyles({
	chatBody: {
		padding: '16px',
	},

	messagesContainer: {
		overflowY: 'auto',
		display: 'flex',
		flexDirection: 'column',
		padding: '6px',
		maxHeight: '80vh',
	},
})
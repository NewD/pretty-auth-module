import React, { ReactElement, useEffect } from 'react'
import { Box, Paper, Typography } from '@material-ui/core'
import { Message } from '../message'
import { chatStyles } from './style'
import { SendBlock } from '../send-block'
import { useSelector } from 'react-redux'
import { AppState } from '../../../../redux/reducers/rootReducer'
import { useWS } from '../../providers/websocket'

export function ChatContainer(): ReactElement {
	const classes = chatStyles()
	const { messages } = useSelector((state: AppState) => state.chat)

	const ws = useWS()

	useEffect(() => {
		if (ws) {
			ws.onopen = function (e) {
				console.log('ws is open')
			}
		}
	}, [ws])

	return (
		<Paper className={classes.chatBody}>
			<Box className={classes.messagesContainer}>
				{messages.length ? (
					messages.map((message, index) => {
						return (
							<Message
								author={message.author}
								receiver={message.receiver}
								text={message.text}
								date={message.date}
								isOwn={message.isOwn}
								key={index}
							/>
						)
					})
				) : (
					<Typography>No messages yet!</Typography>
				)}
			</Box>

			<SendBlock />
		</Paper>
	)
}

import { makeStyles } from '@material-ui/core'

export const textAreaStyles = makeStyles({
	textArea: {
		resize: 'none',
		padding: '10px',
		width: '100%',
		borderRadius: '6px',
		border: '1px solid #ccc',
	},

	areaWrapper: {
		width: '100%',
		padding: '12px',
		margin: '0 auto',
		display: 'flex',
		justifyContent: 'center',
	},
})

import React, { Dispatch, SetStateAction, useEffect, useRef } from 'react'
import { Box, TextareaAutosize } from '@material-ui/core'
import { textAreaStyles } from './style'

interface IProps {
	setUpMessage: Dispatch<SetStateAction<string>>
	retContentNow: boolean
}

export function ChatInputField({ setUpMessage, retContentNow }: IProps) {
	const classes = textAreaStyles()
	const textAreaRef = useRef<HTMLTextAreaElement>(null)

	useEffect(() => {
		if (retContentNow && textAreaRef.current && textAreaRef.current.value) {
			setUpMessage(textAreaRef.current.value)
			textAreaRef.current.value = ''
		}
	}, [retContentNow, setUpMessage])

	return (
		<Box className={classes.areaWrapper}>
			<TextareaAutosize
				ref={textAreaRef}
				className={classes.textArea}
				rowsMin="2"
				rowsMax="8"
				style={{ resize: 'none' }}
			/>
		</Box>
	)
}

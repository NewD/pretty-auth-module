import React, { ReactElement, useEffect, useState } from 'react'
import { Box, Button } from '@material-ui/core'
import { ChatInputField } from '../chat-input-field'
import { useDispatch } from 'react-redux'
import { Dispatch } from 'redux'
import { ChatActionTypes } from '../../redux/types'
import { sendMessageAction } from '../../redux/actions'
import { useWS } from '../../providers/websocket'

export function SendBlock(): ReactElement {
	const [messageText, setMessageText] = useState<string>('')
	const [isSending, setIsSending] = useState<boolean>(false)
	const dispatch = useDispatch<Dispatch<ChatActionTypes>>()

	const handleSendMessage = async () => {
		setIsSending(true)
	}
	const ws = useWS()

	useEffect(() => {
		console.log(isSending, messageText)

		if (isSending && messageText) {
			console.log('here', isSending, messageText)

			if (ws)
				dispatch(
					sendMessageAction(
						{
							author: 'Newd',
							receiver: 'Chel',
							text: messageText,
							date: new Date(),
							isOwn: true,
						},
						ws
					)
				)

			setIsSending(false)
			setMessageText('')
		}
	}, [messageText, isSending, dispatch, ws])

	return (
		<Box>
			<ChatInputField setUpMessage={setMessageText} retContentNow={isSending} />
			<Button onClick={handleSendMessage}>Send!</Button>
		</Box>
	)
}

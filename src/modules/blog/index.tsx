import React from 'react'
import { ReactElement } from 'react'
import { IModule } from '../../interfaces/module'
import { Box, Button, Container } from '@material-ui/core'
import { Link, Route } from 'react-router-dom'
import { PostCreateForm } from './components/post-create-form'
import { PostsList } from './components/posts-list'

export const blogModule: IModule = {
	component: Blog,
	name: 'blog',
	path: '/blog',
}

function Blog(): ReactElement {
	return (
		<Container maxWidth="md">
			<Route path="/blog" exact>
				<Link to="/blog/create-post">
					<Button variant="contained" color="primary">
						Add post
					</Button>
				</Link>

				<Box>
					<PostsList  />
				</Box>
			</Route>

			<Route path="/blog/create-post">
				<PostCreateForm />
			</Route>
		</Container>
	)
}

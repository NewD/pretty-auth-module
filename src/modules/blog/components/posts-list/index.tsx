import React, { ReactElement } from 'react'
import { Box } from '@material-ui/core'
import { IPost } from '../../interfaces/post'
import { Post } from '../post'

interface IProps {
	posts: Array<IPost>
}

export function PostsList({ posts }: IProps): ReactElement {
	return (
		<Box>
			{posts.map(post => (
				<Post {...post} />
			))}
		</Box>
	)
}

import { Card, CardActions, CardContent, CardHeader, CardMedia } from '@material-ui/core'
import React, { ReactElement } from 'react'
import { IPost } from '../../interfaces/post'

export function Post({ content }: IPost): ReactElement {
	return (
		<Card>
			<CardHeader>POST Title</CardHeader>

			<CardMedia title="post image" />

			<CardContent>Post content!</CardContent>

			<CardActions>Actions</CardActions>
		</Card>
	)
}

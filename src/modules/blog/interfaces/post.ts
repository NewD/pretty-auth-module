export interface IPost {
	title: string
	body: Array<string> | string
	userId: string
	date: Date
	viewers: number
}

import React, { useState, createContext, ReactElement, useContext } from 'react'

interface IContextContent {
	token: string | null
	signIn: (token: string) => void
	logout: () => void
}

const AuthContext = createContext<IContextContent | null>(null)

interface IProps {
	children: ReactElement
}

function AuthProvider({ children }: IProps) {
	const currentToken = localStorage.getItem('token')
	const [token, setToken] = useState<string | null>(currentToken ?? null)

	const signIn = (token: string): void => {
		setToken(token)
		localStorage.setItem('token', token)
	}

	const logout = () => {
		setToken(null)
		localStorage.removeItem('token')
	}

	return (
		<AuthContext.Provider value={{ token, signIn, logout }}>
			{children}
		</AuthContext.Provider>
	)
}

export const useAuth = () => useContext(AuthContext)

export default AuthProvider

export interface IAuthConfig {
	client_id: string
	redirect_uri?: string
}

export interface IFormField {
	name: string
	label: string
	type: string
	validate?: () => void
}

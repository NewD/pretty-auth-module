import { makeStyles } from '@material-ui/core'

export const formStyles = makeStyles({
	wrapper: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		maxWidth: '300px',
		marginBottom: '20px',
		
	},

	element: {
		marginBottom: '20px',
		width: '100%',
	},
})

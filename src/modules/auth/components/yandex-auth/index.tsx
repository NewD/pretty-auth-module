import React, { useEffect } from 'react'
import { Button, CircularProgress } from '@material-ui/core'
import { useYandexAuth } from './hooks/useYandexAuth'
import { IAuthConfig } from '../../interfaces/auth-config'

function YandexAuth() {
	const config: IAuthConfig = {
		client_id: process.env.REACT_APP_YANDEX_CLIENT_ID ?? '',
		redirect_uri: process.env.REACT_APP_MY_HOST ?? '',
	}

	const { yandexSignIn, loading, userData } = useYandexAuth(config)

	useEffect(() => {
		const getUser = async (userData: string) => {
			// for backend: https://yandex.ru/dev/passport/doc/dg/reference/request.html

			const response = await fetch('/api/auth/yandex', {
				method: 'POST',
				body: JSON.stringify({ userData }),
				headers: { 'Content-Type': 'application/json;' },
			})

			const result = await response.json()

			return result
		}

		if (userData) {
			getUser(userData)
		}
	}, [userData])

	return (
		<Button onClick={yandexSignIn} color="primary" variant="contained" disabled={loading}>
			<CircularProgress
				color="primary"
				style={{
					position: 'absolute',
					transition: 'opacity .5s',
					opacity: loading ? 1 : 0,
				}}
				size={20}
			/>
			Yandex
		</Button>
	)
}

export default YandexAuth

import { useEffect, useState } from 'react'
import { IAuthConfig } from '../../../interfaces/auth-config'

export function useYandexAuth(config: IAuthConfig) {
	const [loading, setLoading] = useState(false)
	const [userData, setUserData] = useState<string>('')
	const accessHash = window.location.hash.substring(1)

	const yandexSignIn = () => {
		setLoading(true)
		setTimeout(() => {
			window.location.href = `https://oauth.yandex.ru/authorize?response_type=token&client_id=${config.client_id}& redirect_uri=${config.redirect_uri}& force_confirm=yes& state=newd11116666& display=popup`
		}, 1000)
	}

	useEffect(() => {
		if (!accessHash) return

		setLoading(false)

		const currentUrl = new URL(`?${accessHash}`, config.redirect_uri)
		const token = currentUrl.searchParams.get('access_token') ?? ''
		setUserData(token)

		window.history.replaceState({}, '', config.redirect_uri)

		if (currentUrl.searchParams.get('error')) throw new Error('Error while VK auth')
	}, [accessHash, config.redirect_uri, loading])

	return { yandexSignIn, loading, userData }
}

import React from 'react'
import { Box, Button, TextField, Typography } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { formStyles } from '../../commonStyles/formStyle/style'
import { useRequest } from '../../../../hooks/useRequest'
import { useAuth } from '../../providers/auth'
import { authType } from '../../../../core/App'

interface IProps {
	toRegister: (authType: authType) => void
}

function LoginForm({ toRegister }: IProps) {
	const { request, loading } = useRequest() // ! error is not work!

	const { register, handleSubmit } = useForm()
	const classes = formStyles()
	const auth = useAuth()

	async function loginReq(data: object) {
		const answer = await request('/api/auth/login', 'POST', data)

		auth?.signIn(answer.token)
	}

	return (
		<form
			className={classes.wrapper}
			onSubmit={handleSubmit(async data => await loginReq(data))}
		>
			{loading ? <Typography variant="h5">...Loading...</Typography> : null}

			<Typography className={classes.element} variant="h5">
				Sign in
			</Typography>

			<TextField
				className={classes.element}
				name="login"
				variant="outlined"
				label="login"
				inputRef={register}
			/>

			<TextField
				className={classes.element}
				name="password"
				type="password"
				variant="outlined"
				label="password"
				inputRef={register}
			/>

			<Box style={{ display: 'flex', justifyContent: 'space-around', width: '100%' }}>
				<Button type="submit" variant="contained">
					Sign In
				</Button>

				<Button onClick={() => toRegister('register')}>Sign Up</Button>
			</Box>
		</form>
	)
}

export default LoginForm

import React, { useEffect } from 'react'
import { Box, Typography } from '@material-ui/core'
// import { useAuth } from '../../providers/auth'
import GoogleAuth from '../google-auth'
import VkAuth from '../vk-auth'
import YandexAuth from '../yandex-auth'
import GitHubAuth from '../github-auth'

function SideAuth() {
	// const auth = useAuth()

	useEffect(() => {}, [])

	return (
		<Box>
			<Typography>Enter with help of:</Typography>

			<GoogleAuth />
			<VkAuth />
			<YandexAuth />
			<GitHubAuth />

			<div id="vk_auth"></div>
		</Box>
	)
}

export default SideAuth

import { useEffect, useState } from 'react'
import { IAuthConfig } from '../../../interfaces/auth-config'

export function useGitHubAuth(config: IAuthConfig) {
	const [loading, setLoading] = useState<boolean>(false)
	const currentUrl: URL = new URL(window.location.href)
	const code: string = currentUrl.searchParams.get('code') ?? ''

	useEffect(() => {
		if (code) {
			setLoading(false)
			window.history.replaceState({}, '', config.redirect_uri)
		}
	}, [code, config])

	const gitHubAuth = () => {
		setLoading(true)
		setTimeout(() => {
			window.location.href = `https://github.com/login/oauth/authorize?client_id=${config.client_id}&redirect_uri=${config.redirect_uri}`
		}, 1000)
	}

	return { gitHubAuth, code, loading }
}

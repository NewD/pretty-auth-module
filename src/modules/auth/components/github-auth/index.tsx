import React, { useEffect } from 'react'
import { Button, CircularProgress } from '@material-ui/core'
import { useGitHubAuth } from './hooks/useGitHubAuth'
import { IAuthConfig } from '../../interfaces/auth-config'

function GitHubAuth() {
	const authConfig: IAuthConfig = {
		client_id: process.env.REACT_APP_GITHUB_CLIENT_ID ?? '',
		redirect_uri: process.env.REACT_APP_MY_HOST ?? '',
	}

	const { gitHubAuth, code, loading } = useGitHubAuth(authConfig)

	useEffect(() => {
		const getUser = async () => {
			try {
				const response = await fetch('/api/auth/github', {
					method: 'POST',
					body: JSON.stringify({ code }),
					headers: { 'Content-Type': 'application/json;' },
				})

				const result = await response.json()

				return result
			} catch (err) {
				console.log(err)
			}
		}

		if (code) {
			getUser()
		}
	}, [code])

	// for backend: https://docs.github.com/en/free-pro-team@latest/developers/apps/authorizing-oauth-apps#parameters-1

	return (
		<Button variant="contained" color="primary" onClick={gitHubAuth} disabled={loading}>
			<CircularProgress
				color="primary"
				style={{
					position: 'absolute',
					transition: 'opacity .5s',
					opacity: loading ? 1 : 0,
				}}
				size={20}
			/>
			GitHub
		</Button>
	)
}

export default GitHubAuth

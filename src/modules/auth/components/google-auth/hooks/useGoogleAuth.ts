import { useCallback, useEffect, useRef, useState } from 'react'
import { IAuthConfig } from '../../../interfaces/auth-config'

interface ILoading {
	scriptLoading: boolean
	clientLoading: boolean
}

export function useGoogleInit(config: IAuthConfig) {
	const [googleAuth, setGoogleAuth] = useState<Promise<boolean> | null>(null)
	const [user, setUser] = useState<any>()
	const [loading, setLoading] = useState<ILoading>({
		scriptLoading: true,
		clientLoading: false,
	})

	const googleScript = useRef(document.createElement('script'))
	googleScript.current.src = 'https://apis.google.com/js/platform.js'
	googleScript.current.defer = true

	useEffect(() => {
		document.head.append(googleScript.current)
	}, [googleScript])

	googleScript.current.onload = async () => {
		;(window as any).gapi.load('auth2', async () => {
			try {
				const googleAuth = await (window as any).gapi.auth2.init({
					...config,
				})

				setGoogleAuth(googleAuth)
				setLoading(prev => ({ ...prev, scriptLoading: false }))
			} catch (err) {
				console.log(err, 'no Google auth!')
			}
		})
	}

	const googleSignIn = useCallback(() => {
		try {
			if (!loading.clientLoading) setLoading(prev => ({ ...prev, clientLoading: true }))

			if (loading.scriptLoading) return

			setUser(null)

			if (googleAuth) {
				const googleAuth = (window as any).gapi.auth2.getAuthInstance()

				googleAuth
					.signIn({
						scope: 'profile email',
					})
					.then((googleUser: any) => {
						setUser(googleUser)
					})
					.then(() => setLoading(prev => ({ ...prev, clientLoading: false })))
			}
		} catch (err) {
			console.log(err)
			setLoading(prev => ({ ...prev, clientLoading: false }))
		}
	}, [googleAuth, loading])

	useEffect(() => {
		if (loading.clientLoading && googleAuth) {
			googleSignIn()
			console.log(googleAuth, loading)
		}
	}, [loading, googleAuth, googleSignIn])

	return { user, googleSignIn, loading: loading.clientLoading }
}

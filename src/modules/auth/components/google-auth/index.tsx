import React, { useEffect } from 'react'
import { Button, CircularProgress } from '@material-ui/core'
import { useGoogleInit } from './hooks/useGoogleAuth'
import { IAuthConfig } from '../../interfaces/auth-config'

function GoogleAuth() {
	const authConfig: IAuthConfig = {
		client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID ?? '',
		redirect_uri: process.env.REACT_APP_MY_HOST ?? '',
	}

	const { user, googleSignIn, loading } = useGoogleInit(authConfig)

	useEffect(() => {
		if (user) {
			let id_token = user.getAuthResponse().id_token
			fetch('/api/auth/google-auth', {
				method: 'POST',
				body: JSON.stringify({ token: id_token }),
				headers: { 'Content-Type': 'application/json' },
			})
		}
	}, [user])

	return (
		<Button variant="contained" color="primary" onClick={googleSignIn} disabled={loading}>
			<CircularProgress
				className="load"
				color="primary"
				style={{
					position: 'absolute',
					transition: 'opacity .5s',
					opacity: loading ? 1 : 0,
				}}
				size={20}
			/>
			G
		</Button>
	)
}

export default GoogleAuth

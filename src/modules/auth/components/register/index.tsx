import React from 'react'
import { Box, Button, TextField, Typography } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { formStyles } from '../../commonStyles/formStyle/style'
import { useRequest } from '../../../../hooks/useRequest'
import { useAuth } from '../../providers/auth'
import { authType } from '../../../../core/App'

interface IProps {
	toLogin: (authType: authType) => void
}

function RegisterForm({ toLogin }: IProps) {
	const { request, loading, error } = useRequest()
	const { register, handleSubmit } = useForm()
	const classes = formStyles()
	const auth = useAuth()

	async function registerReq(data: object) {
		try {
			const answer = await request('/api/auth/registration', 'POST', data)
			console.log(error)

			auth?.signIn(answer.token)
		} catch (err) {
			console.log(err, error)
		}
	}

	return (
		<form
			className={classes.wrapper}
			onSubmit={handleSubmit(data => {
				registerReq(data)
			})}
		>
			{loading ? <Typography variant="h5">...Loading...</Typography> : null}

			<Typography className={classes.element} variant="h5">
				Sign up
			</Typography>

			<TextField
				className={classes.element}
				name="login"
				variant="outlined"
				label="login"
				inputRef={register}
			/>

			<TextField
				className={classes.element}
				name="password"
				type="password"
				variant="outlined"
				label="password"
				inputRef={register}
			/>

			<TextField
				className={classes.element}
				name="name"
				variant="outlined"
				label="name"
				inputRef={register}
			/>

			<TextField
				className={classes.element}
				name="phone"
				variant="outlined"
				label="phone"
				inputRef={register}
			/>

			<Box style={{ display: 'flex', justifyContent: 'space-around', width: '100%' }}>
				<Button type="submit" variant="contained">
					Sign Up
				</Button>

				<Button onClick={() => toLogin('login')}>Sign In</Button>
			</Box>
		</form>
	)
}

export default RegisterForm

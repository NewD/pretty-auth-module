import React, { useEffect } from 'react'
import { Button, CircularProgress } from '@material-ui/core'
import { useVkAuth } from './hooks/useVkAuth'
import { IAuthConfig } from '../../interfaces/auth-config'

function VkAuth() {
	const config: IAuthConfig = {
		client_id: process.env.REACT_APP_VK_CLIENT_ID ?? '',
		redirect_uri: process.env.REACT_APP_MY_HOST ?? '',
	}
	const { loading, vkSignIn, userData } = useVkAuth(config)

	useEffect(() => {
		async function getVkAuth(): Promise<void> {
			// TODO refactor getUser away!

			try {
				const response = await fetch('/api/auth/vk', {
					method: 'POST',
					body: JSON.stringify({ ...userData }),
					headers: { 'Content-Type': 'application/json;' },
				})

				const user = await response.json()

				console.log(user)
			} catch (err) {
				throw err
			}
		}

		if (userData.token && userData.userId) {
			getVkAuth()

			console.log('send request!', userData)
		}
	}, [userData])

	return (
		<Button onClick={vkSignIn} color="primary" variant="contained" disabled={loading}>
			<CircularProgress
				color="primary"
				style={{
					position: 'absolute',
					transition: 'opacity .5s',
					opacity: loading ? 1 : 0,
				}}
				size={20}
			/>
			VK
		</Button>
	)
}

export default VkAuth

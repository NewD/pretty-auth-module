import { useEffect, useState, useCallback } from 'react'
import { IAuthConfig } from '../../../interfaces/auth-config'

interface IUserData {
	token: string
	userId: string
}

export function useVkAuth(config: IAuthConfig) {
	const accessHash: string = window.location.hash.substring(1) ?? ''

	const [userData, setUserData] = useState<IUserData>({
		token: '',
		userId: '',
	})
	const [loading, setLoading] = useState<boolean>(false)

	const vkSignIn = useCallback(() => {
		if (!loading) setLoading(true)

		setTimeout(() => {
			window.location.href = `https://oauth.vk.com/authorize?client_id=${config.client_id}&display=popup&redirect_uri=${config.redirect_uri}&scope=email&response_type=token&v=5.120&state=newd111666`
		}, 1000)
	}, [config, loading])

	useEffect(() => {
		if (loading) {
			vkSignIn()
		}
	}, [loading, vkSignIn])

	useEffect(() => {
		if (!loading || !accessHash) return
		console.log(accessHash)

		const currentUrl = new URL(`?${accessHash}`, config.redirect_uri)
		window.history.replaceState({}, '', config.redirect_uri)

		if (currentUrl.searchParams.get('error')) throw new Error('Error while VK auth')

		setUserData(prev => ({
			...prev,
			token: currentUrl.searchParams.get('access_token') ?? '',
			userId: currentUrl.searchParams.get('user_id') ?? '',
		}))
	}, [accessHash, config.redirect_uri, loading]);

	return { vkSignIn, userData, loading: loading }
}

import { Box, Typography } from '@material-ui/core'
import React, { useState } from 'react'
import LoginForm from './components/login'
import RegisterForm from './components/register'
import SideAuth from './components/side-auth'

export const authModule = {
	path: '/auth',
	component: AuthModule,
	name: 'auth',
}

export function AuthModule() {
	const [authType, setAuthType] = useState('login')

	const changeAuthType = () => {
		const newAuthType = authType === 'login' ? 'register' : 'login'
		setAuthType(newAuthType)
	}

	return (
		<>
			<Typography variant="h4">Authentication</Typography>

			<Box style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
				{authType === 'login' ? (
					<LoginForm toRegister={changeAuthType} />
				) : (
					<RegisterForm toLogin={changeAuthType} />
				)}

				<SideAuth />
			</Box>
		</>
	)
}

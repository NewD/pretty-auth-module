import { Button, Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-router-dom'
import { IModule } from '../../interfaces/module'

export const homeModule: IModule = {
	path: '/',
	component: HomeModule,
	name: 'home',
	exact: true,
}

function HomeModule() {
	return (
		<>
			<Typography variant="h4">Home</Typography>
			<Link to="/auth">
				<Button variant="contained" color="primary">
					Auth
				</Button>
			</Link>

			<Link to="/chat">
				<Button variant="contained" color="primary">
					Chat
				</Button>
			</Link>

			<Link to="/blog">
				<Button variant="contained" color="primary">
					Blog
				</Button>
			</Link>
		</>
	)
}

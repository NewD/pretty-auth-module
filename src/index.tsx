import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './core/App'
import AuthProvider from './modules/auth/providers/auth'
import { createStore, compose } from 'redux'
import { Provider } from 'react-redux'
import { rootReducer } from './redux/reducers/rootReducer'

declare global {
	interface Window {
		__REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
	}
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(rootReducer, composeEnhancers())

const app = (
	<React.StrictMode>
		<AuthProvider>
			<Provider store={store}>
				<App />
			</Provider>
		</AuthProvider>
	</React.StrictMode>
)

ReactDOM.render(app, document.getElementById('root'))

import { useState } from 'react'

export function useRequest() {
	const [loading, setLoading] = useState(false)
	const [error, setError] = useState(null)

	const request = async (url: string, method: string, data: object) => {
		try {
			const body = JSON.stringify(data)
			const headers = { 'Content-Type': 'application/json' }

			setLoading(true)
			const response = await fetch(url, { method, body, headers })
			const result = await response.json()

			if (!response.ok) {
				throw new Error(result.message)
			}

			setLoading(false)

			return result
		} catch (err) {
			setLoading(false)
			setError(err.message)
			throw err
		}
	}

	return {
		request,
		loading,
		error,
	}
}
